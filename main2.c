#include<SDL/SDL.h>
#include<malloc.h>
#include<math.h>
#include <stdbool.h>
#include <stdlib.h>

#define WIDTH 640
#define HEIGHT 480
#define DYX 25*WIDTH/320
#define NBITS 32
#define SPOKES 1200
#define SPOKES2 600
#define XCENTER (WIDTH/2)
#define YCENTER ((HEIGHT - HEIGHT/2)/2)
#define TEXTUREWIDTH 13

struct MyScreen {
    SDL_Surface *screen;
    unsigned long half_width,half_height;
};

void spokes_precomp (float *spokeCosCalc, float *spokeSinCalc) {
	int i;
    static float tempConst1=M_PI / (float)SPOKES2;
    static float spokeCalc;

    for (i = 0; i < SPOKES; i++)
	{
		spokeCalc = i * tempConst1;
        spokeCosCalc[i] = cos(spokeCalc);
        spokeSinCalc[i] = sin(spokeCalc);
	}
    return;
}

void wormImg_precomp (char *wormImg, float *spokeCosCalc, float *spokeSinCalc) {
	int i, j;
    static float tempConst1=(float)WIDTH / (float)SPOKES2;
    static float tempConst2=(float)HEIGHT / (float)SPOKES2;
    float x, y, z, divCalcX, divCalcY;
	
	for (j = 1; j < SPOKES2 + 1; j++)
	{
		z = -1.0 + (log(2.0 * j / SPOKES2));

		divCalcX = (float)(j * tempConst1);
        divCalcY = (float)(j * tempConst2);

		for (i = 0; i < SPOKES; i++)
		{
			x = divCalcX * spokeCosCalc[i];
            y = divCalcY * spokeSinCalc[i];

			y -= (DYX * z);

			x += XCENTER;
            y += YCENTER;

			if ((x >= 0) && (x < WIDTH) && (y >= 0) && (y < HEIGHT))
				wormImg[(int)x + (int)y * WIDTH] = (char)((i / 8) % TEXTUREWIDTH + (
				                                       TEXTUREWIDTH * ((j / 7) %
				                                       TEXTUREWIDTH) & 0xff)
                );
		}
	}
    return;
}

bool escape(SDL_Event event) {
    bool result = false;
    if (SDL_PollEvent(&event)) // on check le clavier, si on presse une touche on sort
        if ((event.type == SDL_QUIT) || (event.type == SDL_KEYDOWN))
            result = true;
    return result;
}

SDL_Surface * get_screen () {
    SDL_Surface *screen;
    // sdl is initialised to provide a screen in gfx mode
    SDL_Init(SDL_INIT_VIDEO);
    screen = SDL_SetVideoMode(WIDTH, HEIGHT, NBITS, SDL_HWSURFACE | SDL_DOUBLEBUF);
    return screen;
}

int main(void)
{
	SDL_Event event;

    struct MyScreen my_screen;

	SDL_Rect pixel;
	// int shiftX, shiftY;
	static char wormImg[WIDTH*HEIGHT];
	static float spokeCosCalc[SPOKES], spokeSinCalc[SPOKES];
	
	static unsigned short texture[] = {
    0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,
    0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,
    0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,
    0x3b77,0x3b77,0x3b77,0x4c1c,0x4c1c,0x3b77,0x21ed,0x21ed,0x21ed,0x21ed,
    0x21ed,0x21ed,0x21ed,0x21ed,0x21ed,0x21ed,0x21ed,0x3b77,0x4c1c,0x4c1c,
    0x3b77,0x21ed,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,
    0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,0x3b77,0x21ed,0x08a5,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0000,0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,
    0x3b77,0x21ed,0x08a5,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
    0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,0x3b77,0x21ed,0x08a5,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0000,0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,
    0x3b77,0x21ed,0x08a5,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
    0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,0x3b77,0x21ed,0x08a5,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0000,0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,
    0x3b77,0x21ed,0x08a5,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,
    0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,0x3b77,0x21ed,0x08a5,0x0000,0x0000,
    0x0000,0x0000,0x0000,0x0000,0x0000,0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,
    0x3b77,0x21ed,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,0x08a5,
    0x08a5,0x21ed,0x3b77,0x4c1c,0x4c1c,0x3b77,0x21ed,0x21ed,0x21ed,0x21ed,
    0x21ed,0x21ed,0x21ed,0x21ed,0x21ed,0x21ed,0x21ed,0x3b77,0x4c1c,0x4c1c,
    0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,0x3b77,
    0x3b77,0x3b77,0x3b77,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,
    0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c,0x4c1c
};
	
	pixel.w = pixel.h = 2;
	
	int i, j, temp;

    my_screen.screen = get_screen();
	
    spokes_precomp(spokeCosCalc, spokeSinCalc);
	
	wormImg_precomp(wormImg, spokeCosCalc, spokeSinCalc);
	
    // main render loop
    while (1) {
        if (escape(event))
            break;
		for(pixel.y = 0; pixel.y < HEIGHT; pixel.y += 2)
		{
			for(pixel.x = 0; pixel.x < WIDTH; pixel.x += 2)
			{
				SDL_FillRect(my_screen.screen, &pixel, texture[wormImg[pixel.y * WIDTH + pixel.x]]);
			}
		}
		
		SDL_Flip(my_screen.screen);
		
		// Shifts texture right
		for(j = 0; j < TEXTUREWIDTH; j++)
		{
			temp = texture[(j+1) * TEXTUREWIDTH - 1];
			for(i = TEXTUREWIDTH - 2; i > -1; i--)
			{
				texture[j * TEXTUREWIDTH + i + 1] = texture[j * TEXTUREWIDTH + i];
			}
			texture[j * TEXTUREWIDTH] = temp;
		}
		
		// Shift texture up
		for(i = 0; i < TEXTUREWIDTH; i++)
		{
			temp = texture[i];
			for(j = 1; j < TEXTUREWIDTH; j++)
			{
				texture[(j-1) * TEXTUREWIDTH + i] = texture[j * TEXTUREWIDTH +i];
			}
			texture[TEXTUREWIDTH * TEXTUREWIDTH - TEXTUREWIDTH + i] = temp;
		}
        SDL_Delay(25);
	}
	
	SDL_Quit();
	
	return 0;
}