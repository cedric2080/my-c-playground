#include <SDL/SDL.h>
#include <malloc.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

// for linux https://linux.die.net/man/3/sleep
// for windows use sdtio.h or Sleep from <windows.h>
// This is for the example, POSIX recommends now nano_sleep() from time.h
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#define WIDTH 700
#define HEIGHT 700
#define PI 3.14159265359
#define N_STEPS 1000
#define N_COLORS 1024

struct TableTrigo {
    double *_sin;
    double *_cos;
};

struct MyScreen {
    SDL_Surface *screen;
    unsigned long half_width,half_height;
};

double distance (unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2) {
    // triangle trigonometry
    unsigned int i = x1 - x2, j = y1 - y2;
    return sqrt(i*i+j*j);
}

void trigo_precomp (struct TableTrigo table_trigo) {
    unsigned int x=0;
    // sine/cosine tables pre-computation
    for (x=0; x<360*N_STEPS; x++) {
        table_trigo._sin[x] = floor(sin(x*PI/180/N_STEPS)*N_STEPS)/N_STEPS;
        table_trigo._cos[x] = floor(cos(x*PI/180/N_STEPS)*N_STEPS)/N_STEPS;
    }
    return;
}

void create_palette (unsigned long *pal, double deg, unsigned int n_colors) {
    unsigned int x=0;
    // colors palette
    for (x=0; x<n_colors; x++) {
        pal[x] = (unsigned long)(
            ((unsigned char)(96+95*sin(x*0.6*deg))) << 16 |
            ((unsigned char)(128+127*sin(x*0.5*deg))) << 8 |
            ((unsigned char)(192+63*cos(x*0.1*deg)))
        );
    }
    return;
}

void go_to_bed(float some_time) {
    //sleep some time in microseconds
    #ifdef _WIN32
    Sleep(some_time);
    #else
    usleep(some_time);
    #endif
    return;
}

bool escape(SDL_Event event) {
    bool result = false;
    if (SDL_PollEvent(&event)) // on check le clavier, si on presse une touche on sort
        if ((event.type == SDL_QUIT) || (event.type == SDL_KEYDOWN))
            result = true;
    return result;
}

SDL_Surface * get_screen () {
    SDL_Surface *screen;
    // sdl is initialised to provide a screen in gfx mode
    SDL_Init(SDL_INIT_VIDEO);
    screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    return screen;
}

void redraw_screen(struct MyScreen my_s, unsigned long *pal, unsigned int n_colors, unsigned int t, double deg, struct TableTrigo table_trigo){
    // main snippet: we scan the screen and calculate the color of each pixel from the palette
    unsigned int x,y =0;
    unsigned long c;
    double f1, f2, f3;
    double *_sin=table_trigo._sin;
    double *_cos=table_trigo._cos;

    for (y=0;y<HEIGHT;y++) {
            for (x=0;x<WIDTH;x++) {
                // the 3 functions are still using expensive sinus/cos functions in the end
                f1 = sin(distance(x, y, WIDTH + WIDTH * _sin[(t*2)%(360*N_STEPS)], HEIGHT + (my_s.half_height * _sin[(t)%(360*N_STEPS)])) * deg * 0.3);
                f2 = cos(distance(x, y, WIDTH + my_s.half_width * _sin[(t*3)%(360*N_STEPS)], HEIGHT + (HEIGHT * _cos[(t*4)%(360*N_STEPS)])) * deg * 0.7);
                f3 = sin(distance(x, y, WIDTH + my_s.half_width * _cos[(t)%(360*N_STEPS)], HEIGHT + (my_s.half_height * _cos[(t*2)%(360*N_STEPS)])) * deg * 1.1);
                // summation and average of the 3 functions, then animation with t parameter -> lookup the color for each pixel in pal
                c = pal[((unsigned char)(n_colors/2 + n_colors/2 * ((f1+f2+f3)/3))+t)%n_colors];
                // putpixel
                *(unsigned long *)(my_s.screen->pixels + y * my_s.screen->pitch + x * my_s.screen->format->BytesPerPixel) = c;
            }
    }
    return;
}

int main (int argc, char *argv[] ) {
    // Let's init some variables
    unsigned int n_colors;
    n_colors = N_COLORS;
    if (argc>1)
        n_colors = atoi(argv[1]);
    if (n_colors%2 != 0)
        n_colors = N_COLORS;

    struct MyScreen my_screen;
    my_screen.half_width=WIDTH/2;
    my_screen.half_height=HEIGHT/2;
    unsigned long pal[n_colors];

    SDL_Event event;
    
    unsigned int t =0;
    double deg = PI/180;

    struct TableTrigo table_trigo;
    table_trigo._sin = calloc(360*N_STEPS,sizeof(double));
    table_trigo._cos = calloc(360*N_STEPS,sizeof(double));
    if (!table_trigo._sin || !table_trigo._cos) return -1; // early escape in case malloc has trouble

    printf("Let's do some precomputations !\n");
    printf("With %d colors.\n", n_colors);
    printf("(Request number from command line:%s)\n", argv[1]);

    trigo_precomp(table_trigo);
    create_palette(pal, deg, n_colors);
    
    printf("Let's start main loop !\n");
    go_to_bed(100*1000); // 0.
    
    my_screen.screen = get_screen();

    // main render loop
    while (1) {
        if (escape(event))
            break;

        // screen surface lock when necessary
        if (SDL_MUSTLOCK(my_screen.screen))
            SDL_LockSurface(my_screen.screen);

        redraw_screen(my_screen, pal, n_colors, t, deg, table_trigo);

        // if screen was locked, we unlock
        if (SDL_MUSTLOCK(my_screen.screen))
            SDL_UnlockSurface(my_screen.screen);

        t += N_STEPS;
        // screen update
        SDL_UpdateRect(my_screen.screen,0,0,0,0);
    }

    // on quitte proprement
    SDL_Quit();
    free (table_trigo._cos);
    free (table_trigo._sin);
    return 0;
}